#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <fcntl.h>
#include <sys/signal.h>

#define STD_INPUT 0
#define STD_OUTPUT 1
#define STD_ERROR 2
#define EXITCMD "exit"
#define CHDIRCMD "cd"
typedef enum {false, true } bool;

struct CharList{
    int nowLength;
    int totalSize;
    char ** content;    
};

struct CharList toBeRemove;
char *home="/root/";
char *HOME=NULL;
char *PROFILE="/root/.mshProfile";
char *promptSign="$";
int BUFFERSIZE=300;

bool parse(char * line, struct CharList *comArgs);
bool doAct(struct CharList comArgs);
bool ifValid(struct CharList comArgs);
void delCharList(struct CharList *comArgs);
void* mMalloc(size_t s);

int main(){
	char *line=(char*)mMalloc(sizeof(char)*BUFFERSIZE);
	char *a;
	struct CharList comArgs;
    HOME=(char*)mMalloc(sizeof(char)*(strlen(home)+1));	
    strcpy(HOME,home);
	chdir(HOME);
 /*   signal(SIGINT,SIG_IGN);                 //not end with ctrl-c or ctrl-/
    signal(SIGQUIT,SIG_IGN);
    signal(SIGTERM,SIG_IGN);
    signal(SIGTSTP,SIG_IGN);  */
	while(1){
        
       // pr();                             //still working on it
		printf("%s",promptSign);
		
		if(fgets(line,BUFFERSIZE,stdin)==NULL){ 
            //fflush(stdin);
            clearerr(stdin);                //not end with ctrl-d
            printf("\n");
            continue;
        }
		if(parse(line,&comArgs)==false){
            fprintf(stderr,"Some error happen in parse()\n");
            continue;
        }
        if(comArgs.content[0]==NULL){            
	       delCharList(&comArgs);	
	       continue;
        }        
        if( !ifValid(comArgs) )
        {
            fprintf(stderr,"Command invalid, wrong \">\" or \"|\"\n");  
            continue;
        }   
    /*    int i; 
        for(i=0;i<comArgs.nowLength-1;i++)
            printf("%s\n",comArgs.content[i]);*/
        doAct(comArgs);
	    delCharList(&comArgs);	
    }
	return 0;
} 

void mGetLine(char **line){
    
}    

void pr(){
  /*  if(access(PROFILE,F_OK)==-1)
        return;
    File *fp;
    if( (fp=fopen(PROFILE,"rt"))==NULL){
        fprintf(stderr,"Cannot open profile (/root/.mshProfile) \n"); 
        return;
    }
    long int fSize;
    fseek(fp,0,SEEK_END)
    fSize=ftell(fp);
    fseek(fp,0,SEEK_SET);*/
    
}

void* mMalloc(size_t s){
	void* rp=NULL;
	rp=malloc(s);
	if(rp!=NULL)
		return rp;
	fprintf(stderr,"Out of memory\n");
	exit(1);
}

void delCharList(struct CharList *comArgs){
    int i;
	for(i=0;i<comArgs->nowLength;i++){
        if(comArgs->content[i]!=NULL)
            free(comArgs->content[i]);
    }
    free(comArgs->content);
    comArgs->content=NULL; 
    comArgs->totalSize=comArgs->nowLength=0;
}

bool ifValid(struct CharList comArgs){
    int i;
    int state;             //0=begin; 1=normal; 2=">"; 4="|" ;
    state=0;
    for(i=0;i<comArgs.nowLength;i++){
        if(comArgs.content[i]==NULL)    continue;
        if(comArgs.content[i][0]=='>'){
            if(state!=1)
                return false;
            state=2;
        }
        else if(comArgs.content[i][0]=='|'){
            if(state!=1)
                return false;
            state=4;
        }
        else{            
            state=1;
        }
    }
    if(state==1 || state==0)
        return true;
    return false;
}

int exeProgram(char **cArgs, int argsSize){     //exec a program
	char **nullTD;
	char *zStr;
	int execReturn;
    
    execReturn=execvp(cArgs[0],cArgs);
    free(cArgs);  
    return execReturn;  
}

bool setSTDOUT(char *outfile){                  //change the stand output
    if(outfile!=NULL){
        int stdo=open(outfile, O_WRONLY|O_CREAT|O_TRUNC, 0777);
        if(stdo<0/* || stdo>=OPEN_MAX*/){
            fprintf(stderr, "Error happened in open output redirection file\n");
            return false;
        }
        close(STD_OUTPUT);
        if(dup(stdo)==-1){
            fprintf(stderr,"Error happened in output redirection\n");
            return false;
        }
        close(stdo);
        free(outfile);
        outfile=NULL;
    }
    return true;
}

char* dirMix(char *a, char *b){
    int n=0;
    if(b[strlen(b)-1]!='/')
        n++;
    char *c=(char *)mMalloc(strlen(a)+strlen(b)+1+n);
    strcpy(c,a);
    strcat(c,b);
    if(n==1)
        strcat(c,"/");        
    return c;
}

void simpleHOME(){
    
}

bool doAct(struct CharList comArgs){
        
	int i,j;
	int state;             //1=normal; 2="|" 
	int prestate;            //pre state. 1=normal; 2="|"
	int execReturn;	
	char * outfile;
	int argsSize;
	int * childExitSts;
	int fd[2];
	int fdi;
	pid_t pid;
	
    prestate=1;  
    
    while(1){                                               //deal with each commands splited by "|"
        outfile=NULL;	
        state=1;
        argsSize=0;	
        for(i=0;i<comArgs.nowLength;i++){                   //find a command with no "|" in the midst of it
            if(comArgs.content[i]==NULL)    continue;
            if(comArgs.content[i][0]=='>'){
                free(comArgs.content[i]);
                comArgs.content[i]=NULL;
                if(outfile!=NULL)   free(outfile);
                outfile=comArgs.content[i+1];
                comArgs.content[i+1]=NULL;             
                i++;
            }
            else if(comArgs.content[i][0]=='|'){
                state=2;
                free(comArgs.content[i]);
                comArgs.content[i]=NULL;
                break;
            }
            else
                argsSize++;
        }         
        char **cArgs=(char **)mMalloc(sizeof(char*)*(argsSize+1));
        int tempi;
        for(i=0,j=0;j<argsSize;i++){                            //copy this command from comArgs into cArgs
            if(comArgs.content[i]!=NULL){
                cArgs[j++]=comArgs.content[i];
                tempi=i+1;
            }
        }
        cArgs[argsSize]=NULL;       
        
        if(strcmp(cArgs[0],EXITCMD)==0) {                        //shell exit when user type EXITCMD
            printf("Goodbye!\n");
            free(cArgs);
            exit(0);
        }
        else if(strcmp(cArgs[0],CHDIRCMD)==0) {                   //deal with "cd"
            char *cz=NULL;
            if(cArgs[1][0]!='/'){
                cz=dirMix(HOME,cArgs[1]);
            }            
            cz=dirMix("",cArgs[1]);
            if(chdir(cz)==-1){
                fprintf(stderr, "cd : can't cd to %s\n",cArgs[1]);
                free(cArgs);
                free(cz);
                return false;
            }
            else{
                free(HOME);
                HOME=cz;
                simpleHOME();
            }
        }        
        else if(state==1){                                           //normal, it's the command with no "|" after it in comArgs
            if( (pid=fork()) != 0 ){
                if(prestate==2)
                    close(fdi);
    			waitpid(pid,&childExitSts,0);
    			printf("exit status : %d\n",childExitSts);
            }
            else{
                if(!setSTDOUT(outfile))                         //change stdoutput depends on ">"
                    exit(1);                    
                if(prestate==2){                                //change stdinput depends on "|"
                    close(STD_INPUT);
                    dup(fdi);
                    close(fdi);
                }               
                if(exeProgram(cArgs,argsSize)==-1){              //execute the child program
                    fprintf(stderr, "Exec1 error\n");
                    exit(1);
                }
                exit(0);
            }
        }
        else if(state==2){                                          //pipe, it's the command with "|" after it in comArgs       
            if( pipe(&fd[0])!=0 ){
                fprintf(stderr,"Pipe error\n");
                free(cArgs);
                return false;
            }
            if( (pid=fork()) != 0 ){
                close(fd[1]);            
                if(prestate==2)
                    close(fdi);
                fdi=fd[0];
    			waitpid(pid,&childExitSts,0);
    			printf("exit status : %d\n",childExitSts);
            }
            else{
                if(!setSTDOUT(outfile))         //change stdoutput depends on ">"
                    exit(1);                    
                close(fd[0]);                   //change stdoutput depends on "|"
                close(STD_OUTPUT);
                dup(fd[1]);
                close(fd[1]);
                if(prestate==2){                //execute the child program
                    close(STD_INPUT);
                    dup(fdi);
                    close(fdi);
                }              
                if(exeProgram(cArgs,argsSize)==-1){
                    fprintf(stderr, "Exec2 error\n");
                    exit(1);
                }
                exit(0);
            } 
        }      
        free(cArgs); 
        for(;tempi<comArgs.nowLength && comArgs.content[tempi]==NULL;tempi++);      
        if(tempi==comArgs.nowLength)                                                //if no command remained in comArgs
            break;
        comArgs.content+=tempi;                                                     //pop the command from comArgs
        comArgs.nowLength-=tempi;
        comArgs.totalSize-=tempi;	         
        prestate=state;
    }
    if(state==2)
        close(fdi);
    return true;
}

bool isBlank(char c) {
    return (c=='\x20' || c=='\t' || c=='\n' || c=='\r' || c=='\f' || c=='\v');
}

bool isSpec(char c) {
    return (c=='>' || c=='|');
}   

bool pushIntoCharList(struct CharList *cl_rel, char *s) {
    int i;
    if( !(cl_rel->nowLength < cl_rel->totalSize-1) ){
        cl_rel->totalSize*=2;
        char **nsl=(char **)mMalloc(sizeof(char*)*cl_rel->totalSize);
        if(nsl==NULL)   
            return false;
        for(i=0;i<cl_rel->nowLength;i++)
            nsl[i]=cl_rel->content[i];
        free(cl_rel->content);
        cl_rel->content=nsl;
    }
    cl_rel->content[cl_rel->nowLength]=s;
    cl_rel->nowLength++;
    return true;
} 

bool addComArg(struct CharList *cl_rel, char *begin, int s){    
    char *comp=NULL;
    comp=(char *)mMalloc(sizeof(char)*(s+1));
    strncpy(comp,begin,s);
    comp[s]='\0';
    pushIntoCharList(cl_rel,comp);
    return true;  
}

bool parse(char * line, struct CharList *comArgs) {         //split the command in to char**,char** is comArgs.content,with a NULL in the comArgs.content[comArgs.nowLength]
    if(line == NULL)    
        return true;
    char a;
    int start=0,end=0;
    int i;    
    comArgs->nowLength=0;
    comArgs->totalSize=20;                                         
    comArgs->content=(char **)mMalloc(sizeof(char*)*comArgs->totalSize);
    for(i=0,a=line[0];;){
        if(a=='\0'){            
            if(start!=end)
                addComArg(comArgs,line+start,end-start);
            pushIntoCharList(comArgs, NULL);
            break;
        }
        else if(isBlank(a)){
            if(start!=end)
                addComArg(comArgs,line+start,end-start);
            start=end=end+1;        
        } 
        else if(a=='|'){
            if(start!=end)
                addComArg(comArgs,line+start,end-start);
            addComArg(comArgs,line+end,1);    
            start=end=end+1;    
        } 
        else if(a=='>'){
            if(start!=end)
                addComArg(comArgs,line+start,end-start);
            addComArg(comArgs,line+end,1);    
            start=end=end+1;  
        }
        else{
            end++;
        }
        i++;
        a=line[i];
    }      
    return true;     
}

